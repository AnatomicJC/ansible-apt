---
- name: Gather the package facts
  ansible.builtin.package_facts:
    manager: apt
  when: ansible_facts.packages is not defined

- name: Import some custom stuff
  ansible.builtin.import_tasks: 10-custom-stuff.yml

- name: Set geo-localized repositories
  when: apt_main_url_localized
  block:
    - name: Get geolocation data from ipinfo.io
      ansible.builtin.uri:
        url: "http://ipinfo.io/json"
        return_content: true
        method: GET
      check_mode: false
      register: apt_ipinfo

    - name: Set apt repository URL based on country (Ubuntu)
      ansible.builtin.set_fact:
        apt_main_url: "{{ (apt_ipinfo.json.country | lower).replace('gb', 'uk') }}.archive.ubuntu.com"
      when: ansible_distribution == 'Ubuntu'

    - name: Set apt repository URL based on country (Debian)
      ansible.builtin.set_fact:
        apt_main_url: "ftp.{{ (apt_ipinfo.json.country | lower).replace('gb', 'fr') }}.debian.org"
      when: ansible_distribution == 'Debian'

- name: Configure /etc/apt/preferences.d/ files
  ansible.builtin.copy:
    dest: "{{ item.dest }}"
    content: "{{ item.content }}"
    owner: root
    group: root
    mode: "0644"
  loop: "{{ apt_preferences }}"
  when: apt_preferences|length > 0

- name: Add 32-bit architecture
  ansible.builtin.copy:
    content: "amd64\ni386\n"
    dest: /var/lib/dpkg/arch
    owner: root
    group: root
    mode: "0644"
  when: apt_32_bit|bool

- name: Ensure /usr/share/keyrings exists
  ansible.builtin.file:
    path: /usr/share/keyrings
    state: directory
    mode: "0755"
    owner: root
    group: root

- name: Add apt keys
  ansible.builtin.copy:
    content: "{{ item.content }}"
    dest: "/usr/share/keyrings/{{ item.name }}.asc"
    owner: root
    group: root
    mode: "0644"
  loop: "{{ apt_keys }}"
  no_log: true

- name: Remove sources.list file
  ansible.builtin.file:
    path: "{{ item.path }}"
    state: "{{ item.state }}"
    owner: root
    group: root
    mode: "{{ item.mode | default('0644') }}"
  loop:
    - path: /etc/apt/sources.list
      state: absent
    - path: /etc/apt/sources.list~
      state: absent
    - path: /etc/apt/sources.list.save
      state: absent
    - path: /etc/apt/sources.list.d
      state: directory
      mode: "0755"
    - path: /etc/apt/sources.list.d/backports.list
      state: absent
    - path: /etc/apt/sources.list.d/main.list
      state: absent
    - path: /etc/apt/sources.list.d/security.list
      state: absent
    - path: /etc/apt/sources.list.d/debian.sources
      state: absent
  tags: apt-sources-list-config

- name: Create sources.list.d main files
  ansible.builtin.template:
    src: "etc/apt/sources.list.d/{{ item }}.sources.j2"
    dest: "/etc/apt/sources.list.d/{{ item }}.sources"
    owner: root
    group: root
    mode: "0644"
  loop: "{{ apt_sources_list }}"
  tags: apt-sources-list-config
  notify:
    - Apt update

- name: Create sources.list.d custom files
  ansible.builtin.copy:
    dest: "/etc/apt/sources.list.d/{{ item.dest }}"
    content: "{{ item.content }}"
    owner: root
    group: root
    mode: "0644"
  loop: "{{ apt_custom_sources_list }}"
  tags: apt-sources-list-config
  notify:
    - Apt update

- name: Flush handlers
  ansible.builtin.meta: flush_handlers

- name: Additional packages
  ansible.builtin.import_tasks: 20-additional-packages.yml
